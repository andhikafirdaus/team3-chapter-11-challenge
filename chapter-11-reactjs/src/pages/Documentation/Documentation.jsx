import React, { useState } from "react";
import { ControlBar, Player } from "video-react";
import "video-react/dist/video-react.css";
import { Button } from "reactstrap";
import video from "./game.mp4";
import PDF from "./pdf";

export default function Documentation() {
  const [player, setPlayer] = useState();
  const [source, setSource] = useState(video);

  const play = () => {
    player.play();
  };

  const pause = () => {
    player.pause();
  };

  const load = () => {
    player.load();
  };

  return (
    <div>
      <h1 className="ms-3 mt-5">Strategi Permainan</h1>
      <Player
        ref={(player) => {
          setPlayer(player);
        }}
      >
        <source src={source}></source>
        <ControlBar autoHide></ControlBar>
      </Player>
      <div className="py-3">
        <Button onClick={play} className="ms-3">
          Play
        </Button>
        <Button onClick={pause} className="ms-3">
          Pause
        </Button>
      </div>
      <div className="py-3"></div>
      <h1 className="ms-3 mt-5">Aturan Permainan</h1>
      <div className="col mx-auto">
        <PDF/>
      </div>
    </div>
  );
}
